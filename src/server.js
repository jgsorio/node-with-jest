const express = require('express');
const path = require('path');

const server = express();
const router = express.Router();

router.get('/', (req, res) => {
    return res.sendFile(path.resolve(__dirname, 'index.html'));
});

server.use(router);

server.listen(3333, () => console.log('Server is running'));
