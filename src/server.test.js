const fs = require('fs');
const path = require('path');

test('expected index file to be present', () => {
    expect(fs.existsSync(path.resolve(__dirname, 'index.html'))).toBeTruthy();
});

test('expected Dockerfile to be present', () => {
    expect(fs.existsSync(path.resolve(__dirname, '..', 'Dockerfile'))).toBeTruthy();
});

test('expected package.json to be present', () => {
    expect(fs.existsSync(path.resolve(__dirname, '..', 'package.json'))).toBeTruthy();
});
